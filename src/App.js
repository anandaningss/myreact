import React, { Component } from 'react';
import './index.css';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
  } from "react-router-dom";
import '@elastic/eui/dist/eui_theme_light.css';

//Pages
import mainPage from "./pages"
import loginPage from "./pages/login";
import signupPage from "./pages/signup"
import daftarMahasiswa from "./pages/daftar-mahasiswa"

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={mainPage} />
          <Route exact path="/login" component={loginPage} />
          <Route exact path="/signup" component={signupPage} />
          <Route exact path="/daftar-mahasiswa" component={daftarMahasiswa} />
          <Redirect to="/404" />
        </Switch>
      </Router>
    )
  }
}

export default App;
