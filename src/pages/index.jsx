import React from 'react';
import hero from '../img/hero.jpg';
import saveMoney from '../img/piggy-bank.svg';
import kelasKaryawan from '../img/education.svg';
import facility from '../img/school.svg';
import '../index.css';
import {
    EuiFlexGroup,
    EuiFlexItem,
    EuiButton,
    EuiCard
  } from '@elastic/eui';
import '@elastic/eui/dist/eui_theme_light.css';
import { EuiTitle } from '@elastic/eui';
import { EuiSpacer } from '@elastic/eui';


const Index = () => {
  return (
    <div>
      <Header />
      <Hero />
      <WhyTitle />
      <ThreeAdvantage />
      <CpTitle />
      <ContactPerson />
      <Footer />
    </div>
  );
};

const Header = () => {
    return(
      <EuiFlexGroup id="header" gutterSize='none' justifyContent="spaceAround">
        <EuiFlexItem grow={false} className='logoPlaceholder'>
          <img class="logo" src="logo.png" alt="logo kampus"></img>
        </EuiFlexItem>
        <EuiFlexItem grow={false} className='headerPlaceholder'>
          <div className='headerLink'>
            <EuiButton href="/login" className="loginButton">Masuk</EuiButton>
            <EuiButton href="/signup" className="registerButton" fill>Daftar</EuiButton>
          </div>
        </EuiFlexItem>
      </EuiFlexGroup>
    );
}

const Hero = () => {
  return(
    <EuiFlexGroup id="heroPlaceholder">
    <EuiFlexItem>
      <div className='hero'>
        <img src={hero} className='heroImages' alt="hero images"></img>
      </div>
    </EuiFlexItem>
  </EuiFlexGroup>
  );
}

const WhyTitle = () => {
  return(
    <EuiFlexGroup justifyContent="spaceAround" id="whyTitle">
    <EuiFlexItem grow={false}>
      <EuiSpacer />
      <EuiTitle size="l">
        <h1>Mengapa kuliah di Unindra?</h1>
      </EuiTitle>
      <EuiSpacer />
    </EuiFlexItem>
  </EuiFlexGroup>
  );
}

const ThreeAdvantage = () => {
  return(
    <EuiFlexGroup justifyContent="spaceAround" id="columnContainer">
    <EuiFlexItem>
      <EuiCard
        icon={<img src={saveMoney} alt="save money" className="iconAdvantage" />}
        title=""
        description=""
        footer={
          <p style={{fontSize: 20}}>Biaya Terjangkau</p>
        }
      />
    </EuiFlexItem>
    <EuiFlexItem>
      <EuiCard
          icon={<img src={facility} alt="save money" className="iconAdvantage" />}
          title=""
          description=""
          footer={
            <p style={{fontSize: 20}}>Fasilitas Lengkap</p>
          }
        />
    </EuiFlexItem>
    <EuiFlexItem>
      <EuiCard
            icon={<img src={kelasKaryawan} alt="save money" className="iconAdvantage" />}
            title=""
            description=""
            footer={
              <p style={{fontSize: 20}}>Tersedia Kelas Karyawan</p>
            }
          />
    </EuiFlexItem>
  </EuiFlexGroup>
  )
}

const CpTitle = () => {
  return(
    <EuiFlexGroup justifyContent="spaceAround" id="cpTitle">
      <EuiFlexItem grow={false}>
        <EuiSpacer />
        <EuiTitle size="l">
          <h1>Butuh informasi? Silahkan hubungi kami</h1>
        </EuiTitle>
        <EuiSpacer />
      </EuiFlexItem>
    </EuiFlexGroup>
  );
}

const ContactPerson = () => {
  return(
    <EuiFlexGroup justifyContent="center" id="noHpPlaceholder">
      <EuiFlexItem grow={false} className="noHp">
        <p className="noHp">WhatsApp : +62 896-5524-6028</p>
      </EuiFlexItem>
    </EuiFlexGroup>
  );
}

const Footer = () => {
  return(
      <EuiFlexGroup id="footer" gutterSize="none" justifyContent="spaceAround">
        <EuiFlexItem grow={false} className='footerPlaceholder'>
          <div className='footerLink'>
            <a href="/" className="link">Beranda</a>
          </div>
        </EuiFlexItem>
        <EuiFlexItem grow={false} className='footerPlaceholder'>
          <div className='footerLink'>
            <a href="/" className="link">Kebijakan Privasi</a>
          </div>
        </EuiFlexItem>
        <EuiFlexItem grow={false} className='footerPlaceholder'>
          <div className='footerLink'>
            <a href="/" className="link">Tentang Kami</a>
          </div>
        </EuiFlexItem>
      </EuiFlexGroup>
  );
}


export default Index;
