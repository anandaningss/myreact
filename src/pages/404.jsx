import React from 'react';
import '../index.css';
import {
    EuiFlexGroup,
    EuiFlexItem,
    EuiButton,
  } from '@elastic/eui';
import '@elastic/eui/dist/eui_theme_light.css';
import { EuiTitle } from '@elastic/eui';

const Login = () => {
    return(
        <div>
            <Header />
            <NotFound />
            <Footer />
        </div>
    );
};

const Header = () => {
    return(
      <EuiFlexGroup id="header" gutterSize='none' justifyContent="spaceAround">
        <EuiFlexItem grow={true} className='logoPlaceholder'>
          <img class="logo" src="logo.png" alt="logo kampus"></img>
        </EuiFlexItem>
        <EuiFlexItem grow={true} className='headerPlaceholder'>
          <div className='headerLink'>
            <EuiButton href="" className="loginButton">Masuk</EuiButton>
            <EuiButton href="" className="registerButton" fill>Daftar</EuiButton>
          </div>
        </EuiFlexItem>
      </EuiFlexGroup>
    );
}

const NotFound = () => {
    return(
      <EuiFlexGroup id="notFound" gutterSize='none' justifyContent="spaceAround">
        <EuiFlexItem grow={true} className='loginFormPlaceholder'>
            <EuiTitle>
                <h1>
                    404 Error Not Found
                </h1>
            </EuiTitle>
        </EuiFlexItem>
      </EuiFlexGroup>
    );
}

const Footer = () => {
    return(
      <EuiFlexGroup id="footer" gutterSize="none" justifyContent="spaceAround">
        <EuiFlexItem grow={false} className='footerPlaceholder'>
          <div className='footerLink'>
            <a href="/" className="link">Beranda</a>
          </div>
        </EuiFlexItem>
        <EuiFlexItem grow={false} className='footerPlaceholder'>
          <div className='footerLink'>
            <a href="/" className="link">Kebijakan Privasi</a>
          </div>
        </EuiFlexItem>
        <EuiFlexItem grow={false} className='footerPlaceholder'>
          <div className='footerLink'>
            <a href="/" className="link">Tentang Kami</a>
          </div>
        </EuiFlexItem>
      </EuiFlexGroup>
    );
  }

export default Login;