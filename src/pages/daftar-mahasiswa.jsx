import React from 'react';
import '../index.css';
import {
    EuiFlexGroup,
    EuiFlexItem,
    EuiButton,
  } from '@elastic/eui';
import '@elastic/eui/dist/eui_theme_light.css';
import { EuiTitle } from '@elastic/eui';

const Login = () => {
    return(
        <div>
            <Header />
            <ListTitle />
            <ListTable />
            <Footer />
        </div>
    );
};

const Header = () => {
    return(
      <EuiFlexGroup id="header" gutterSize='none' justifyContent="spaceAround">
        <EuiFlexItem grow={false} className='logoPlaceholder'>
          <img class="logo" src="logo.png" alt="logo kampus" />
        </EuiFlexItem>
        <EuiFlexItem grow={false} className='headerPlaceholder'>
          <div className='headerLink'>
            <EuiButton href="/" className="logoutButton" color="danger" fill>Keluar</EuiButton>
          </div>
        </EuiFlexItem>
      </EuiFlexGroup>
    );
}

const ListTitle = () => {
    return(
        <EuiFlexGroup justifyContent="spaceAround" id="welcomeLogin">
            <EuiFlexItem grow={false}>
                <EuiTitle size="l">
                    <h1>Daftar Mahasiswa</h1>
                </EuiTitle>
            </EuiFlexItem>
        </EuiFlexGroup>
    );
}

const ListTable = () => {
    return(
        <EuiFlexGroup justifyContent="spaceAround" id="listTable">
            <EuiFlexItem grow={false}>
              <div>
                <div class="euiTableHeaderMobile">
                  <div class="euiFlexGroup euiFlexGroup--gutterLarge euiFlexGroup--alignItemsBaseline euiFlexGroup--justifyContentSpaceBetween euiFlexGroup--directionRow">
                    <div class="euiFlexItem euiFlexItem--flexGrowZero"></div>
                    <div class="euiFlexItem euiFlexItem--flexGrowZero"></div>
                  </div>
                </div>
                <table class="euiTable euiTable--responsive">
                  <caption class="euiScreenReaderOnly euiTableCaption" role="status" aria-relevant="text"
                  aria-live="polite"></caption>
                  <thead>
                    <tr>
                      <th class="euiTableHeaderCell euiTableHeaderCell--hideForMobile" scope="col"
                      role="columnheader" data-test-subj="tableHeaderCell_firstName_0">
                        <div class="euiTableCellContent">
                          <span class="euiTableCellContent__text">No.</span>
                        </div>
                      </th>
                      <th class="euiTableHeaderCell euiTableHeaderCell--hideForMobile" scope="col"
                      role="columnheader" data-test-subj="tableHeaderCell_lastName_1">
                        <div class="euiTableCellContent">
                          <span class="euiTableCellContent__text">Nama</span>
                        </div>
                      </th>
                      <th class="euiTableHeaderCell" scope="col" role="columnheader" data-test-subj="tableHeaderCell_github_2">
                        <div class="euiTableCellContent">
                          <span class="euiTableCellContent__text">Alamat</span>
                        </div>
                      </th>
                      <th class="euiTableHeaderCell" scope="col" role="columnheader" data-test-subj="tableHeaderCell_dateOfBirth_3">
                        <div class="euiTableCellContent">
                          <span class="euiTableCellContent__text">Tanggal Lahir</span>
                        </div>
                      </th>
                      <th class="euiTableHeaderCell" scope="col" role="columnheader" data-test-subj="tableHeaderCell_nationality_4">
                        <div class="euiTableCellContent">
                          <span class="euiTableCellContent__text">Program Studi</span>
                        </div>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                </table>
              </div>
            </EuiFlexItem>
        </EuiFlexGroup>
    );
}



const Footer = () => {
    return(
      <EuiFlexGroup id="footer" gutterSize="none" justifyContent="spaceAround">
        <EuiFlexItem grow={false} className='footerPlaceholder'>
          <div className='footerLink'>
            <a href="/" className="link">Beranda</a>
          </div>
        </EuiFlexItem>
        <EuiFlexItem grow={false} className='footerPlaceholder'>
          <div className='footerLink'>
            <a href="/" className="link">Kebijakan Privasi</a>
          </div>
        </EuiFlexItem>
        <EuiFlexItem grow={false} className='footerPlaceholder'>
          <div className='footerLink'>
            <a href="/" className="link">Tentang Kami</a>
          </div>
        </EuiFlexItem>
      </EuiFlexGroup>
    );
  }

export default Login;