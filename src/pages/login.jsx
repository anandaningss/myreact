import React from 'react';
import '../index.css';
import {
    EuiFlexGroup,
    EuiFlexItem,
    EuiButton,
    EuiFormLabel,
    EuiFormControlLayout,
    EuiHorizontalRule
  } from '@elastic/eui';
import '@elastic/eui/dist/eui_theme_light.css';
import { EuiTitle } from '@elastic/eui';

const Login = () => {
    return(
        <div>
            <Header />
            <LoginTitle />
            <LoginForm />
            <Footer />
        </div>
    );
};

const Header = () => {
    return(
      <EuiFlexGroup id="header" gutterSize='none' justifyContent="spaceAround">
        <EuiFlexItem grow={false} className='logoPlaceholder'>
          <img class="logo" src="logo.png" alt="logo kampus"></img>
        </EuiFlexItem>
        <EuiFlexItem grow={false} className='headerPlaceholder'>
          <div className='headerLink'>
            <EuiButton href="/login" className="loginButton">Masuk</EuiButton>
            <EuiButton href="/signup" className="registerButton" fill>Daftar</EuiButton>
          </div>
        </EuiFlexItem>
      </EuiFlexGroup>
    );
}

const LoginTitle = () => {
    return(
        <EuiFlexGroup justifyContent="spaceAround" id="welcomeLogin">
            <EuiFlexItem grow={false}>
                <EuiTitle size="l">
                    <h1>Masuk dengan akun anda</h1>
                </EuiTitle>
            </EuiFlexItem>
        </EuiFlexGroup>
    );
}

const LoginForm = () => {
    return(
        <EuiFlexGroup justifyContent="spaceAround" id="loginFormContainer">
            <EuiFlexItem grow={false}>
            <EuiHorizontalRule margin="l" />
            <EuiFormControlLayout
                prepend={<EuiFormLabel htmlFor="textField19">No. Induk</EuiFormLabel>}>
                <input
                    type="number"
                    className="euiFieldText euiFieldText--inGroup"
                    id="textField19"
                />
            </EuiFormControlLayout>
            <EuiHorizontalRule margin="l" />
            <EuiFormControlLayout
                prepend={<EuiFormLabel htmlFor="textField19" className="loginForm">Password</EuiFormLabel>}>
                <input
                    type="password"
                    className="euiFieldText euiFieldText--inGroup"
                    id="textField19"
                />
            </EuiFormControlLayout>
                <EuiHorizontalRule margin="l" />
                <EuiButton fill onClick={() => window.alert('Button clicked')}>
                    Masuk
                </EuiButton>
            </EuiFlexItem>
        </EuiFlexGroup>
    );
}

const Footer = () => {
    return(
      <EuiFlexGroup id="footer" gutterSize="none" justifyContent="spaceAround">
        <EuiFlexItem grow={false} className='footerPlaceholder'>
          <div className='footerLink'>
            <a href="/" className="link">Beranda</a>
          </div>
        </EuiFlexItem>
        <EuiFlexItem grow={false} className='footerPlaceholder'>
          <div className='footerLink'>
            <a href="/" className="link">Kebijakan Privasi</a>
          </div>
        </EuiFlexItem>
        <EuiFlexItem grow={false} className='footerPlaceholder'>
          <div className='footerLink'>
            <a href="/" className="link">Tentang Kami</a>
          </div>
        </EuiFlexItem>
      </EuiFlexGroup>
    );
  }

export default Login;