import React from 'react';
import '../index.css';
import {
    EuiFlexGroup,
    EuiFlexItem,
    EuiButton,
    EuiFormLabel,
    EuiFormControlLayout,
    EuiHorizontalRule,
    EuiDatePicker
  } from '@elastic/eui';
import '@elastic/eui/dist/eui_theme_light.css';
import { EuiTitle } from '@elastic/eui';

const Login = () => {
    return(
        <div>
            <Header />
            <LoginTitle />
            <LoginForm />
            <Footer />
        </div>
    );
};

const Header = () => {
    return(
      <EuiFlexGroup id="header" gutterSize='none' justifyContent="spaceAround">
        <EuiFlexItem grow={false} className='logoPlaceholder'>
          <img class="logo" src="logo.png" alt="logo kampus" />
        </EuiFlexItem>
        <EuiFlexItem grow={false} className='headerPlaceholder'>
          <div className='headerLink'>
            <EuiButton href="/login" className="loginButton">Masuk</EuiButton>
            <EuiButton href="/signup" className="registerButton" fill>Daftar</EuiButton>
          </div>
        </EuiFlexItem>
      </EuiFlexGroup>
    );
}

const LoginTitle = () => {
    return(
        <EuiFlexGroup justifyContent="spaceAround" id="welcomeLogin">
            <EuiFlexItem grow={false}>
                <EuiTitle size="l">
                    <h1>Daftarkan diri anda disini</h1>
                </EuiTitle>
            </EuiFlexItem>
        </EuiFlexGroup>
    );
}

const LoginForm = () => {
    return(
        <EuiFlexGroup justifyContent="spaceAround" id="loginFormContainer">
            <EuiFlexItem grow={false}>
            <EuiHorizontalRule margin="l" />
            <EuiFormControlLayout
                prepend={<EuiFormLabel htmlFor="textField19">Nama</EuiFormLabel>}>
                <input
                    type="text"
                    className="euiFieldText euiFieldText--inGroup"
                    id="textField19"
                />
            </EuiFormControlLayout>
            <EuiHorizontalRule margin="l" />
            <EuiFormControlLayout
                prepend={<EuiFormLabel htmlFor="textField19">Alamat</EuiFormLabel>}>
                <input
                    type="text"
                    className="euiFieldText euiFieldText--inGroup"
                    id="textField19"
                />
            </EuiFormControlLayout>
            <EuiHorizontalRule margin="l" />
            <EuiFormControlLayout
                prepend={<EuiFormLabel htmlFor="textField19">Tanggal Lahir</EuiFormLabel>}>
                <EuiDatePicker />
            </EuiFormControlLayout>
            <EuiHorizontalRule margin="l" />
              <div class="euiFormControlLayout__childrenWrapper">
                <select id="selectDocExample" class="euiSelect" aria-label="Program Studi">
                  <option selected>Pilih Program Studi...</option>
                  <option>Teknik Informatika</option>
                  <option>Sistem Informatika</option>
                  <option>Desain Komunikasi dan Visual</option>
                </select>
                <div class="euiFormControlLayoutIcons euiFormControlLayoutIcons--right">
                  <span class="euiFormControlLayoutCustomIcon">
                    <svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg"
                    class="euiIcon euiIcon--medium euiIcon-isLoading euiFormControlLayoutCustomIcon__icon"
                    focusable="false" role="img" aria-hidden="true" />
                  </span>
                </div>
              </div>
            <EuiHorizontalRule margin="l" />
            <EuiFormControlLayout
                prepend={<EuiFormLabel htmlFor="textField19" className="loginForm">Password</EuiFormLabel>}>
                <input
                    type="password"
                    className="euiFieldText euiFieldText--inGroup"
                    id="textField19"
                />
            </EuiFormControlLayout>
                <EuiHorizontalRule margin="l" />
                <EuiButton fill onClick={() => window.alert('Button clicked')}>
                    Daftar
                </EuiButton>
            </EuiFlexItem>
        </EuiFlexGroup>
    );
}

const Footer = () => {
    return(
      <EuiFlexGroup id="footer" gutterSize="none" justifyContent="spaceAround">
        <EuiFlexItem grow={false} className='footerPlaceholder'>
          <div className='footerLink'>
            <a href="/" className="link">Beranda</a>
          </div>
        </EuiFlexItem>
        <EuiFlexItem grow={false} className='footerPlaceholder'>
          <div className='footerLink'>
            <a href="/" className="link">Kebijakan Privasi</a>
          </div>
        </EuiFlexItem>
        <EuiFlexItem grow={false} className='footerPlaceholder'>
          <div className='footerLink'>
            <a href="/" className="link">Tentang Kami</a>
          </div>
        </EuiFlexItem>
      </EuiFlexGroup>
    );
  }

export default Login;